/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ucb.talp.academia.enums;

/**
 *
 * @author Jonathan
 */
public enum Sexo {
    MASCULINO("Masculino"),
    FEMINIMO("Feminino"),
    INDEFINIDO("Indefinido");

    private Sexo(String sexo) {
        setSexo(sexo);
    }
    
    private String sexo;

    public String getSexo() {
        return sexo;
    }

    private void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
