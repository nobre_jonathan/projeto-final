/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.util.lazy;

import br.ucb.talp.academia.beans.Aluno;
import br.ucb.talp.academia.dao.AlunoDAO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jonathan
 */
public class LazyAlunoDataModel extends LazyDataModel<Aluno> {
    private static final long serialVersionUID = 603576406005233412L;
    
    private List<Aluno> dataSource;

    public LazyAlunoDataModel() {
        updateDataSource();
    }
    
    public void updateDataSource() {
        setDataSource(new AlunoDAO().getAll());
    }
    
    public LazyAlunoDataModel(List<Aluno> dataSource) {
        setDataSource(dataSource);
    }
    
    @Override
    public Aluno getRowData(String rowKey) {
        for (Aluno aluno : getDataSource()) {
            if (aluno.getId().toString().equals(rowKey)) {
                return aluno;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(Aluno object) {
        return object.getId();
    }

    @Override
    public List<Aluno> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<Aluno> data = new ArrayList<Aluno>();
        
        for (Aluno aluno : getDataSource()) {
            Boolean match = true;
            
            if (filters != null) {
                for (Iterator<String> iterator = filters.keySet().iterator(); iterator.hasNext();) {
                    try {
                        String filterProperty = iterator.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(aluno.getClass().getField(filterProperty).get(aluno));
                        
                        if (filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                        } else {
                            match = false;
                            break;
                        }
                        
                    } catch (Exception ex) {
                        Logger.getLogger(LazyAlunoDataModel.class.getName()).log(Level.SEVERE, null, ex);
                        match = false;
                    }
                }
            }
            
            if (match) {
                data.add(aluno);
            }
        }
        
        // Row Count
        int dataSize = data.size();
        setRowCount(dataSize);
        
        // Paginate
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        } else {
            return data;
        }
    }
    
    /* Getters and Setter */

    public List<Aluno> getDataSource() {
        return dataSource;
    }

    public void setDataSource(List<Aluno> dataSource) {
        this.dataSource = dataSource;
    }
}
