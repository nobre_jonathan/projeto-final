/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.util.lazy;

import br.ucb.talp.academia.beans.Instrutor;
import br.ucb.talp.academia.dao.InstrutorDAO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Jonathan
 */
public class LazyInstrutorDataModel extends LazyDataModel<Instrutor> {
    private static final long serialVersionUID = 603576406005233412L;
    
    private List<Instrutor> dataSource;

    public LazyInstrutorDataModel() {
        updateDataSource();
    }
    
    public void updateDataSource() {
        setDataSource(new InstrutorDAO().getAll());
    }
    
    public LazyInstrutorDataModel(List<Instrutor> dataSource) {
        setDataSource(dataSource);
    }
    
    @Override
    public Instrutor getRowData(String rowKey) {
        for (Instrutor instrutor : getDataSource()) {
            if (instrutor.getId().toString().equals(rowKey)) {
                return instrutor;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(Instrutor object) {
        return object.getId();
    }

    @Override
    public List<Instrutor> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<Instrutor> data = new ArrayList<Instrutor>();
        
        for (Instrutor instrutor : getDataSource()) {
            Boolean match = true;
            
            if (filters != null) {
                for (Iterator<String> iterator = filters.keySet().iterator(); iterator.hasNext();) {
                    try {
                        String filterProperty = iterator.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(instrutor.getClass().getField(filterProperty).get(instrutor));
                        
                        if (filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                        } else {
                            match = false;
                            break;
                        }
                        
                    } catch (Exception ex) {
                        Logger.getLogger(LazyInstrutorDataModel.class.getName()).log(Level.SEVERE, null, ex);
                        match = false;
                    }
                }
            }
            
            if (match) {
                data.add(instrutor);
            }
        }
        
        // Row Count
        int dataSize = data.size();
        setRowCount(dataSize);
        
        // Paginate
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        } else {
            return data;
        }
    }
    
    /* Getters and Setter */

    public List<Instrutor> getDataSource() {
        return dataSource;
    }

    public void setDataSource(List<Instrutor> dataSource) {
        this.dataSource = dataSource;
    }
}
