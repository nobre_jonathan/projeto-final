/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ucb.talp.academia.interfaces;

import java.util.List;

/**
 *
 * @author Jonathan
 */
public interface DAO<E> {

    public Boolean add(E entity);
    public Boolean remove(E entity);
    public Boolean update(E entity);
    public List<E> getAll();
    public void close();

}
