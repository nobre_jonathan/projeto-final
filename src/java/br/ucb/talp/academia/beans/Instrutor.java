package br.ucb.talp.academia.beans;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author Jonathan
 */
@Entity ()
@NamedQuery (name = "getAllInstrutor", query = "SELECT i FROM Instrutor i")
public class Instrutor implements Serializable {
    private static final long serialVersionUID = -1602600917371137851L;
    
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column (nullable = false, length = 150)
    private String nome;
    
    @Column (nullable = false, length = 20)
    private String matricula;
    
    @Column (name = "ANO_CONCLUSAO", nullable = false)
    private Integer anoConclusao;

    public Instrutor() {
    }
    
    public Instrutor(String nome, String matricula, Integer anoConclusao) {
        setNome(nome);
        setMatricula(matricula);
        setAnoConclusao(anoConclusao);
    }
    
    /* Getters and Setter */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Integer getAnoConclusao() {
        return anoConclusao;
    }

    public void setAnoConclusao(Integer anoConclusao) {
        this.anoConclusao = anoConclusao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.nome);
        hash = 89 * hash + Objects.hashCode(this.matricula);
        hash = 89 * hash + Objects.hashCode(this.anoConclusao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Instrutor other = (Instrutor) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        if (!Objects.equals(this.anoConclusao, other.anoConclusao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Instrutor{" + "Id: " + getId() + ", nome: " + getNome() + ", Matricula: " + getMatricula() + ", Ano de Conclusao: " + getAnoConclusao() + '}';
    }
}
