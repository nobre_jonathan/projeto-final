package br.ucb.talp.academia.beans;

import br.ucb.talp.academia.enums.Sexo;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author Jonathan
 */
@Entity
@NamedQuery (name = "getAllAluno", query = "SELECT a FROM Aluno a")
public class Aluno implements Serializable {
    private static final long serialVersionUID = -1464879048335403135L;

    @Id @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column (nullable = false, length = 150)
    private String nome;
    
    @Column (length = 20, nullable = false, unique = true)
    private String cpf;
    
    @Enumerated (EnumType.ORDINAL)
    private Sexo sexo;
    
    @OneToOne (cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false, fetch = FetchType.EAGER)
    @JoinColumn (name = "FK_INSTRUTOR")
    private Instrutor instrutor;
    
    @Column (nullable = false)
    private Double altura;
    
    @Column (nullable = false)
    private Double peso;

    public Aluno() {
        setSexo(Sexo.MASCULINO);
        setInstrutor(new Instrutor());
    }
    
    public Aluno(String nome, String cpf, Sexo sexo, Instrutor instrutor, Double altura, Double peso) {
        this();
        setNome(nome);
        setCpf(cpf);
        setSexo(sexo);
        setInstrutor(instrutor);
        setAltura(altura);
        setPeso(peso);
    }
    
    /* Getters and Setter */
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Instrutor getInstrutor() {
        return instrutor;
    }

    public void setInstrutor(Instrutor instrutor) {
        this.instrutor = instrutor;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.nome);
        hash = 23 * hash + Objects.hashCode(this.cpf);
        hash = 23 * hash + Objects.hashCode(this.sexo);
        hash = 23 * hash + Objects.hashCode(this.instrutor);
        hash = 23 * hash + Objects.hashCode(this.altura);
        hash = 23 * hash + Objects.hashCode(this.peso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aluno other = (Aluno) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.cpf, other.cpf)) {
            return false;
        }
        if (this.sexo != other.sexo) {
            return false;
        }
        if (!Objects.equals(this.instrutor, other.instrutor)) {
            return false;
        }
        if (!Objects.equals(this.altura, other.altura)) {
            return false;
        }
        if (!Objects.equals(this.peso, other.peso)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Aluno{" + "Id: " + getId() + ", Nome: " + getNome() + ", CPF: " + getCpf() + ", Sexo: " + getSexo().getSexo() + ", Altura: " + getAltura() + ", Peso: " + getPeso() + ", Instrutor: " + getInstrutor() +'}';
    }
}
