/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.beans;

import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jonathan
 */
public class GerenciadorEntidade {
    
    private static GerenciadorEntidade gerenciadorEntidade = null;
    
    private EntityManagerFactory factory;
    private EntityManager manager;

    private GerenciadorEntidade() {
        setFactory(Persistence.createEntityManagerFactory("ACADEMIA"));
        setManager(getFactory().createEntityManager());
    }
    
    public static GerenciadorEntidade getInstance () {
        if (getGerenciadorEntidade() == null) {
            setGerenciadorEntidade(new GerenciadorEntidade());
        }
        return getGerenciadorEntidade();
    }
    
    public void close () {
        try {
            getManager().close();
            getFactory().close();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).severe(e.getMessage());
        }
        setGerenciadorEntidade(null);
    }
    
    /* Getters and Setter */

    private static GerenciadorEntidade getGerenciadorEntidade() {
        return gerenciadorEntidade;
    }

    private static void setGerenciadorEntidade(GerenciadorEntidade gerenciadorEntidade) {
        GerenciadorEntidade.gerenciadorEntidade = gerenciadorEntidade;
    }

    private EntityManagerFactory getFactory() {
        return factory;
    }

    private void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
    }

    public EntityManager getManager() {
        return manager;
    }

    private void setManager(EntityManager manager) {
        this.manager = manager;
    }
}
