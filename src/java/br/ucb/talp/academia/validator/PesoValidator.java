/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Jonathan
 */
@FacesValidator (value = "pesoValidator")
public class PesoValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) return;
        try {
            Double peso = Double.parseDouble(value.toString());
            if (peso < 0.0 || peso > 300) {
                throw new ValidatorException(new FacesMessage("Peso Inválido!"));
            }
        } catch (NumberFormatException e) {
            throw new ValidatorException(new FacesMessage("Peso Inválido!"));
        }
    }
}
