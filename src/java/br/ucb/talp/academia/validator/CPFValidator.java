/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.validator;

import br.ucb.talp.academia.beans.Aluno;
import br.ucb.talp.academia.dao.AlunoDAO;
import br.ucb.talp.academia.exception.CPFAlreadyExistsException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Jonathan
 */
@FacesValidator (value = "cpfValidator")
public class CPFValidator implements Validator{

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) return;
        String cpf = value.toString();
        for (Aluno aluno : new AlunoDAO().getAll()) {
            if (aluno.getCpf().equals(cpf)) {
                throw new ValidatorException(new FacesMessage(CPFAlreadyExistsException.MESSAGE_ERRO));
            }
        }
    }

}
