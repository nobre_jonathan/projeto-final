/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.exception;

/**
 *
 * @author Jonathan
 */
public class CPFAlreadyExistsException extends Exception {
    private static final long serialVersionUID = 8719822834114108145L;
    public static final String MESSAGE_ERRO = "CPF Já Cadastrado na Base de Dados!";
    
    public CPFAlreadyExistsException() {
        super(MESSAGE_ERRO);
    }
    
    public CPFAlreadyExistsException(String message) {
        super(message);
    }
}
