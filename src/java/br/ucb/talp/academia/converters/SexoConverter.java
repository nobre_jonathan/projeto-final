/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ucb.talp.academia.converters;

import br.ucb.talp.academia.enums.Sexo;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jonathan
 */
@FacesConverter (value = "sexoConverter", forClass = Sexo.class)
public class SexoConverter extends EnumConverter {

    public SexoConverter() {
        super(Sexo.class);
    }
}
