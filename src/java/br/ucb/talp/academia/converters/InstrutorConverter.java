package br.ucb.talp.academia.converters;

import br.ucb.talp.academia.beans.Instrutor;
import br.ucb.talp.academia.dao.InstrutorDAO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.convert.Converter;

/**
 *
 * @author jonathan
 */
@FacesConverter (value = "instrutorConverter")
public class InstrutorConverter implements Converter{


    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null) return null;
        for (Instrutor instrutor : new InstrutorDAO().getAll()) {
            if (instrutor.getId().toString().equals(value)) {
                return instrutor;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) return null;
        Instrutor instrutor = (Instrutor) value;
        return instrutor.getId().toString();
    }
}
