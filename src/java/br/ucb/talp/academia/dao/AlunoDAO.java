package br.ucb.talp.academia.dao;

import br.ucb.talp.academia.interfaces.DAO;
import br.ucb.talp.academia.beans.Aluno;
import br.ucb.talp.academia.beans.GerenciadorEntidade;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jonathan
 */
public class AlunoDAO implements DAO<Aluno> {

    private GerenciadorEntidade gerenciadorEntidade;

    public AlunoDAO() {
        setGerenciadorEntidade(GerenciadorEntidade.getInstance());
    }

    @Override
    public Boolean add(Aluno entity) {
        try {
            getGerenciadorEntidade().getManager().getTransaction().begin();
            getGerenciadorEntidade().getManager().persist(entity);
            getGerenciadorEntidade().getManager().getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ADD: {0}", e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean remove(Aluno entity) {
        try {
            getGerenciadorEntidade().getManager().getTransaction().begin();
            getGerenciadorEntidade().getManager().remove(entity);
            getGerenciadorEntidade().getManager().getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "DELETANDO: {0}", e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean update(Aluno entity) {
        try {
            getGerenciadorEntidade().getManager().getTransaction().begin();
            getGerenciadorEntidade().getManager().merge(entity);
            getGerenciadorEntidade().getManager().getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "UPDATE: {0}", e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    @Override
    public List<Aluno> getAll() {
        TypedQuery<Aluno> query = getGerenciadorEntidade().getManager().createNamedQuery("getAllAluno", Aluno.class);
        return query.getResultList();
    }

    @Override
    public void close() {
        getGerenciadorEntidade().close();
    }
    
    /* Getters and Setter */

    public GerenciadorEntidade getGerenciadorEntidade() {
        return gerenciadorEntidade;
    }

    private void setGerenciadorEntidade(GerenciadorEntidade gerenciadorEntidade) {
        this.gerenciadorEntidade = gerenciadorEntidade;
    }
}
