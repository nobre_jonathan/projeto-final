/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ucb.talp.academia.dao;

import br.ucb.talp.academia.beans.GerenciadorEntidade;
import br.ucb.talp.academia.beans.Instrutor;
import br.ucb.talp.academia.interfaces.DAO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jonathan
 */
public class InstrutorDAO implements DAO<Instrutor> {
    
    private GerenciadorEntidade entityManager;

    public InstrutorDAO() {
        setEntityManager(GerenciadorEntidade.getInstance());
    }

    @Override
    public Boolean add(Instrutor entity) {
        try {
            getEntityManager().getManager().getTransaction().begin();
            getEntityManager().getManager().persist(entity);
            getEntityManager().getManager().getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ADD: {0}", e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean remove(Instrutor entity) {
        try {
            getEntityManager().getManager().getTransaction().begin();
            getEntityManager().getManager().remove(entity);
            getEntityManager().getManager().getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "DELETANDO: {0}", e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean update(Instrutor entity) {
        try {
            getEntityManager().getManager().getTransaction().begin();
            getEntityManager().getManager().merge(entity);
            getEntityManager().getManager().getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "UPDATE: {0}", e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public List<Instrutor> getAll() {
        TypedQuery<Instrutor> query = getEntityManager().getManager().createNamedQuery("getAllInstrutor", Instrutor.class);
        return query.getResultList();
    }

    @Override
    public void close() {
        getEntityManager().close();
    }
    
    /* Getters and Setter */

    private GerenciadorEntidade getEntityManager() {
        return entityManager;
    }

    private void setEntityManager(GerenciadorEntidade entityManager) {
        this.entityManager = entityManager;
    }
}
