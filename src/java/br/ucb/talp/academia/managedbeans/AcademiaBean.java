package br.ucb.talp.academia.managedbeans;

import br.ucb.talp.academia.beans.Aluno;
import br.ucb.talp.academia.beans.Instrutor;
import br.ucb.talp.academia.dao.AlunoDAO;
import br.ucb.talp.academia.dao.InstrutorDAO;
import br.ucb.talp.academia.enums.Sexo;
import br.ucb.talp.academia.exception.CPFAlreadyExistsException;
import br.ucb.talp.academia.interfaces.DAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jonathan
 */
@ManagedBean
@SessionScoped
public class AcademiaBean implements Serializable {
    private static final long serialVersionUID = -8646088973771803910L;
    
    private DAO<Aluno> alunoDAO;
    private InstrutorDAO instrutorDAO;

    private Aluno aluno;
    private Instrutor instrutor;
    
    @PostConstruct ()
    public void init() {
        setAluno(new Aluno());
        setInstrutor(new Instrutor());
        
        setAlunoDAO(new AlunoDAO());
        setInstrutorDAO(new InstrutorDAO());
    }
    
    public String cadastrarAluno() {    
        getAlunoDAO().add(getAluno());
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Cadastrando: {0}", getAluno().toString());
        setAluno(new Aluno());
        return "listagem_alunos.xhtml";
    }
    
    public void validarCPF() {
        for (Aluno aluno : new AlunoDAO().getAll()) {
            if (aluno.getCpf().equals(getAluno().getCpf())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,CPFAlreadyExistsException.MESSAGE_ERRO,null));
            }
        }
    }
    
    public String cadastroInstrutor() {
        getInstrutorDAO().add(getInstrutor());
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Cadastrando: {0}", getInstrutor().toString());
        setInstrutor(new Instrutor());
        return "listagem_instrutores.xhtml";
    }
    
    public List<Sexo> getSexoEnums () {
        List<Sexo> sexos = new ArrayList<Sexo>();
        sexos.addAll(Arrays.asList(Sexo.values()));
       return sexos;
    }
    
    public List<Integer> getAnos() {
        List<Integer> anos = new ArrayList<Integer>();
        Integer anoAtual = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = anoAtual; i >= 1950; i--) {
            anos.add(i);
        }
        return anos;
    }
    
    /* Getters and Setter */

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Instrutor getInstrutor() {
        return instrutor;
    }

    public void setInstrutor(Instrutor instrutor) {
        this.instrutor = instrutor;
    }

    public DAO<Aluno> getAlunoDAO() {
        return alunoDAO;
    }

    public void setAlunoDAO(AlunoDAO alunoDAO) {
        this.alunoDAO = alunoDAO;
    }

    public InstrutorDAO getInstrutorDAO() {
        return instrutorDAO;
    }

    public void setInstrutorDAO(InstrutorDAO instrutorDAO) {
        this.instrutorDAO = instrutorDAO;
    }
}
