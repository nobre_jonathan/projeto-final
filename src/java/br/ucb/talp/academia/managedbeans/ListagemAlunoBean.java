/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ucb.talp.academia.managedbeans;

import br.ucb.talp.academia.beans.Aluno;
import br.ucb.talp.academia.dao.AlunoDAO;
import br.ucb.talp.academia.interfaces.DAO;
import br.ucb.talp.academia.util.lazy.LazyAlunoDataModel;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Jonathan
 */
@ManagedBean
@ViewScoped
public class ListagemAlunoBean implements Serializable {
    private static final long serialVersionUID = 3513844413809726339L;
    
    private LazyDataModel<Aluno> alunoLazy;
    
    private Aluno aluno;
    
    private DAO<Aluno> dao;

    @PostConstruct
    public void init() {
        setAluno(new Aluno());
        setDao(new AlunoDAO());
        refreshData();
    }
    
    public void excluir(Aluno aluno) {
        getDao().remove(aluno);
        refreshData();
    }
    
    public void atualizar() {
        getDao().update(getAluno());
    }
    
    private void refreshData() {
        setAlunoLazy(new LazyAlunoDataModel(getDao().getAll()));
    }
    
    /* Getters and Setter */

    public LazyDataModel<Aluno> getAlunoLazy() {
        return alunoLazy;
    }

    public void setAlunoLazy(LazyDataModel<Aluno> alunoLazy) {
        this.alunoLazy = alunoLazy;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public DAO<Aluno> getDao() {
        return dao;
    }

    public void setDao(DAO<Aluno> dao) {
        this.dao = dao;
    }
}
