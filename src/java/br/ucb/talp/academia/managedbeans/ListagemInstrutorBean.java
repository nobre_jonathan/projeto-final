/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ucb.talp.academia.managedbeans;

import br.ucb.talp.academia.beans.Instrutor;
import br.ucb.talp.academia.dao.InstrutorDAO;
import br.ucb.talp.academia.interfaces.DAO;
import br.ucb.talp.academia.util.lazy.LazyInstrutorDataModel;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Jonathan
 */
@ManagedBean
@ViewScoped
public class ListagemInstrutorBean implements Serializable {
    private static final long serialVersionUID = 3513844413809726339L;
    
    private LazyDataModel<Instrutor> instrutorLazy;
    
    private Instrutor instrutor;
    
    private DAO<Instrutor> dao;

    @PostConstruct
    public void init() {
        setInstrutor(new Instrutor());
        setDao(new InstrutorDAO());
        refreshData();
    }
    
    public void excluir(Instrutor instrutor) {
        getDao().remove(instrutor);
        refreshData();
    }
    
    public void atualizar() {
        getDao().update(getInstrutor());
    }
    
    private void refreshData() {
        setInstrutorLazy(new LazyInstrutorDataModel(getDao().getAll()));
    }
    
    /* Getters and Setter */

    public LazyDataModel<Instrutor> getInstrutorLazy() {
        return instrutorLazy;
    }

    public void setInstrutorLazy(LazyDataModel<Instrutor> instrutorLazy) {
        this.instrutorLazy = instrutorLazy;
    }

    public Instrutor getInstrutor() {
        return instrutor;
    }

    public void setInstrutor(Instrutor instrutor) {
        this.instrutor = instrutor;
    }

    public DAO<Instrutor> getDao() {
        return dao;
    }

    public void setDao(DAO<Instrutor> dao) {
        this.dao = dao;
    }
}
